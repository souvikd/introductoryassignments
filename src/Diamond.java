import java.util.Scanner;

public class Diamond {

    public static void main(String[] args) {

        System.out.println("Enter n.");
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();


        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length - i - 1; j++)
                System.out.print(" ");
            for (int k = 0; k <= 2 * i; k++)
                System.out.print("*");
            System.out.println("");
        }

        for (int i = length - 2; i >= 0; i--) {
            for (int j = 0; j < length - i - 1; j++)
                System.out.print(" ");
            for (int k = 0; k <= 2 * i; k++)
                System.out.print("*");
            System.out.println("");
        }
    }
}
