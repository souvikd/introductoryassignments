import java.util.Scanner;

public class FizzBuzzExercise {

    public static void main(String[] args) {

        System.out.println("Enter limit.");
        Scanner scanner = new Scanner(System.in);
        int limit = scanner.nextInt();
        FizzBuzz(limit);

    }

    public static void FizzBuzz(int limit) {

        for (int i = 1; i <= limit; i++) {

            if (i % 3 == 0)
                System.out.print("Fizz");

            if (i % 5 == 0)
                System.out.print("Buzz");

            if (i % 3 != 0 && i % 5 != 0)
                System.out.print(i);

            System.out.println();
        }
    }

}
