public class PrimeFactors {

    public static void main(String[] args) {

        generate(26);
    }

    public static void generate(int n) {

        int prime;

        for (int factor = 2; factor <= n / 2; factor++) {

            if (n % factor == 0) {
                for (prime = 2; prime < factor; prime++) {
                    if (factor % prime == 0) {
                        break;
                    }
                }
                if (prime == factor) {
                    System.out.println(factor);
                }
            }
        }


    }
}
