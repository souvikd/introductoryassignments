import java.util.Scanner;

public class RightTriangle {

    public static void main(String[] args) {

        System.out.println("Enter n.");
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();

        for (int i = 0; i < length; i++) {

            for (int j = 0; j < i + 1; j++)
                System.out.print("*");
            System.out.println();
        }

    }
}
