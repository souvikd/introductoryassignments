import java.util.Scanner;

public class VerticalLine {

    public static void main(String[] args) {

        System.out.println("Enter n.");
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();

        for (int i = 0; i < length; i++)
            System.out.println("*");

    }
}
